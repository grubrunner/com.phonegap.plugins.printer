var exec = require('cordova/exec');

var Printer = {
    /**
     * Print a Test Receipt
     */
    printTestReceipt : function(success, failure, ipAddress) {
        exec(success, failure, "Printer", "printTestReceipt", [ipAddress]);
    },

    /**
     * Print a Receipt
     */
    printReceipt : function(success, failure, ipAddress, objectJSON) {
        console.log("Debug: printReceipt done")

        exec(success, failure, "Printer", "printReceipt", [ipAddress, JSON.stringify(objectJSON)]);
    }
};

module.exports = Printer;

