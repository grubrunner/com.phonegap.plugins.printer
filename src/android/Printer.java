package com.phonegap.plugins.printer;

import com.phonegap.plugins.printer.MyAsyncTask;
import com.phonegap.plugins.printer.MainActivity;
import com.phonegap.plugins.printer.MainActivitySingleton;
import com.phonegap.plugins.printer.PrintQueue;

import android.content.Context;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import java.lang.InterruptedException;

import java.sql.Timestamp;
import java.util.Date;

import android.os.Looper;
import android.os.Handler;

public class Printer extends CordovaPlugin {
    MainActivity activity;

    /**
     * Constructor.
     */
    public Printer() {
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        try{
            final String ipAddress = args.getString(0);
            String orderString = null;

            try{
              orderString = args.getString(1);
            }catch(Exception e){
              // NOOP
            }

            print(ipAddress, orderString, callbackContext);
            return true;
        }catch(Exception e){
            //TODO: (low priority) Get orderId from orderString, and set the printerStatus to error and printerMessage to "JSON Exception"

            e.printStackTrace();
            return false;
        }
    }

    public void print(final String ipAddress, final String objectJSONString, final CallbackContext callbackContext) {

        try{
            PrintQueue.enqueue(new PrintQueueItem(ipAddress, objectJSONString, callbackContext));
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        this.activity = MainActivitySingleton.getInstance();
        new MyAsyncTask().execute();
    }
}
