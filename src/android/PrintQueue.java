package com.phonegap.plugins.printer;

import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;
import java.lang.InterruptedException;
import com.phonegap.plugins.printer.PrintQueueItem;

public class PrintQueue {
  private static LinkedBlockingQueue<PrintQueueItem> queue = new LinkedBlockingQueue<PrintQueueItem>();
 
  public static PrintQueueItem peek() {
  	return queue.peek();
  }

  public static void enqueue(PrintQueueItem newItem) throws InterruptedException {
  	queue.put(newItem);
  }

  public static PrintQueueItem dequeue() throws InterruptedException {
  	return queue.take();
  }
}