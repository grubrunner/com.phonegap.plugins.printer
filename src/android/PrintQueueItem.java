package com.phonegap.plugins.printer;

import org.apache.cordova.CallbackContext;

public class PrintQueueItem {
  public String order;
  public String ipAddress;
  public CallbackContext context;

  public PrintQueueItem(final String ipAddress, final String newOrderObject, final CallbackContext newContext){
    this.ipAddress = ipAddress;
    this.order = newOrderObject;
    this.context = newContext;
  }
}
