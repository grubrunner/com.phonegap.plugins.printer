package com.phonegap.plugins.printer;

import android.os.AsyncTask;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.widget.ProgressBar;
import com.phonegap.plugins.printer.MainActivitySingleton;
import com.phonegap.plugins.printer.MyBus;
import com.phonegap.plugins.printer.AsyncTaskResultEvent;
import com.phonegap.plugins.printer.PrintQueue;
import com.phonegap.plugins.printer.PrintQueueItem;
import java.util.Set;
import org.apache.cordova.CallbackContext;
import org.json.JSONObject;

public class MyAsyncTask extends AsyncTask<Void, Void, String> {
    private ProgressBar progressBar;
    private boolean isBluetoothPrinter = false;

    public MyAsyncTask(){
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
          for (BluetoothDevice device : pairedDevices) {
           String deviceBTName = device.getName();

            if(deviceBTName != null && deviceBTName.toLowerCase().indexOf("micronics") >= 0){
                this.isBluetoothPrinter = true;
            }
          }
        }else{
          this.isBluetoothPrinter = false;
        }
    }
 
  @Override protected String doInBackground(Void... params) {
    System.out.println("\n\n*** DO IN BACKGROUND");
    
    PrintQueueItem queueItem;
    try{
      queueItem = PrintQueue.dequeue();

      try {
        String orderString = queueItem.order;

        if(orderString == null){
          MainActivitySingleton.getInstance().printTestReceipt(queueItem.ipAddress, queueItem.context);
        }else{
          JSONObject object = new JSONObject(orderString);
          MainActivitySingleton.getInstance().printReceipt(queueItem.ipAddress, object, this.isBluetoothPrinter, queueItem.context);
        }
        
        return "SUCCESS";
      } catch (Exception e) {
        e.printStackTrace();
        return "FAILURE";
      }
    }catch(Exception e){
      return "EXCEPTION: " + e.getMessage();
    }
  }
 
  @Override protected void onPostExecute(String result) {
    System.out.println("\n\n*** MyAsyncTask.onPostExecute");
    System.out.println("\n\n*** Result: " + result);
    MyBus.getInstance().post(new AsyncTaskResultEvent(result));
  }
}
