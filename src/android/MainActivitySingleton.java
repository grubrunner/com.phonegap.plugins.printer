package com.phonegap.plugins.printer;

import com.phonegap.plugins.printer.MainActivity;

public class MainActivitySingleton {
 
  private static final MainActivity activity = new MainActivity();
 
  public static MainActivity getInstance() {
    return activity;
  }
}