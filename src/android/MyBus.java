package com.phonegap.plugins.printer;

import com.squareup.otto.Bus;

public class MyBus {
 
  private static final Bus BUS = new Bus();
 
  public static Bus getInstance() {
  	System.out.println("\n\n*** Get Bus.getInstance");
    return BUS;
  }
}