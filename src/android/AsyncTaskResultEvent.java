package com.phonegap.plugins.printer;

public class AsyncTaskResultEvent {
 
  private String result;
 
  public AsyncTaskResultEvent(String result) {
  	System.out.println("\n\n*** AsyncTaskResultEvent("+result+")");
    this.result = result;
  }
 
  public String getResult() {
  	System.out.println("\n\n*** AsyncTaskResultEvent.getResult");
    return result;
  }
}