package com.phonegap.plugins.printer;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.squareup.otto.Subscribe;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.phonegap.plugins.printer.RasterDocument.RasPageEndMode;
import com.phonegap.plugins.printer.RasterDocument.RasSpeed;
import com.phonegap.plugins.printer.RasterDocument.RasTopMargin;
import com.phonegap.plugins.printer.AsyncTaskResultEvent;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;
import com.starmicronics.stario.StarPrinterStatus;

public class MainActivity extends Activity {
  private static final int TYPEFACE_BOLD = 0x00000001;

  private static final int printableArea = 576; // for raster data
  private static final int TEST_ORDER_ID = 99999999;
  private static final String portSettings = ";l";

  private static final int TEXTSIZE_EXTRA_SMALL = 8;
  private static final int TEXTSIZE_SMALL = 13;
  private static final int TEXTSIZE_MEDIUM = 17;
  private static final int TEXTSIZE_LARGE = 20;
  private static final int TEXTSIZE_LARGER = 24;
  private static final int TEXTSIZE_EXTRA_LARGE = 34;

  private static final int NUM_LINES_SMALL = 36;
  private static final int NUM_LINES_MEDIUM = 28;
  private static final int NUM_LINES_LARGE = 20;
  private static final int NUM_LINES_EXTRA_LARGE = 20;

	public enum NarrowWide {
		_2_6, _3_9, _4_12, _2_5, _3_8, _4_10, _2_4, _3_6, _4_8
	};

	public enum BarCodeOption {
		No_Added_Characters_With_Line_Feed, Adds_Characters_With_Line_Feed, No_Added_Characters_Without_Line_Feed, Adds_Characters_Without_Line_Feed
	}

	public enum Min_Mod_Size {
		_2_dots, _3_dots, _4_dots
	};

	public enum NarrowWideV2 {
		_2_5, _4_10, _6_15, _2_4, _4_8, _6_12, _2_6, _3_9, _4_12
	};

	public enum CorrectionLevelOption {
		Low, Middle, Q, High
	};

	public enum Model {
		Model1, Model2
	};

	public enum Limit {
		USE_LIMITS, USE_FIXED
	};

	public enum CutType {
		FULL_CUT, PARTIAL_CUT, FULL_CUT_FEED, PARTIAL_CUT_FEED
	};

	public enum Alignment {
		Left, Center, Right
	};

	private static byte[] createRasterCommand(String printText, int textSize, int bold) {
		return createRasterCommand(printText, textSize, bold, Layout.Alignment.ALIGN_NORMAL);
	}

	private static byte[] createRasterCommand(String printText, int textSize, int bold, Layout.Alignment align) {
		byte[] command;

		Paint paint = new Paint();
		paint.setStyle(Paint.Style.FILL);
		paint.setColor(Color.BLACK);
		paint.setAntiAlias(true);

		Typeface typeface;

		try {
			typeface = Typeface.create(Typeface.MONOSPACE, bold);
		} catch (Exception e) {
			typeface = Typeface.create(Typeface.DEFAULT, bold);
		}

		paint.setTypeface(typeface);
		paint.setTextSize(textSize * 2);
		paint.setLinearText(true);

		TextPaint textpaint = new TextPaint(paint);
		textpaint.setLinearText(true);
		android.text.StaticLayout staticLayout = new StaticLayout(printText, textpaint, printableArea, align, 1, 0, false);
		int height = staticLayout.getHeight();

		Bitmap bitmap = Bitmap.createBitmap(staticLayout.getWidth(), height, Bitmap.Config.RGB_565);
		Canvas c = new Canvas(bitmap);
		c.drawColor(Color.WHITE);
		c.translate(0, 0);
		staticLayout.draw(c);

		StarBitmap starbitmap = new StarBitmap(bitmap, false, printableArea);

		command = starbitmap.getImageRasterDataForPrinting(true);

		return command;
	}

	private static byte[] convertFromListByteArrayTobyteArray(List<byte[]> ByteArray) {
		int dataLength = 0;
		for (int i = 0; i < ByteArray.size(); i++) {
			dataLength += ByteArray.get(i).length;
		}

		int distPosition = 0;
		byte[] byteArray = new byte[dataLength];
		for (int i = 0; i < ByteArray.size(); i++) {
			System.arraycopy(ByteArray.get(i), 0, byteArray, distPosition, ByteArray.get(i).length);
			distPosition += ByteArray.get(i).length;
		}

		return byteArray;
	}

	private static boolean sendCommand(CallbackContext callbackContext, Context context, int orderId, String portName, String portSettings, ArrayList<byte[]> byteList) throws Exception {
		boolean result = true;
		StarIOPort port = null;
		try {
			try{
				port = StarIOPort.getPort(portName, portSettings, 10000, context);
			}catch(Exception e){
				throw new StarIOPortException("CONNECTION FAILED");
			}

			/* Start of Begin / End Checked Block Sample code */
			StarPrinterStatus status = port.beginCheckedBlock();

			byte[] commandToSendToPrinter = convertFromListByteArrayTobyteArray(byteList);
			port.writePort(commandToSendToPrinter, 0, commandToSendToPrinter.length);

			// port.setEndCheckedBlockTimeoutMillis(30000);// Change the timeout time of endCheckedBlock method.
			status = port.endCheckedBlock();

			if (true == status.coverOpen) {
				throw new StarIOPortException("TRAY NOT CLOSED");
			} else if (true == status.receiptPaperEmpty) {
				throw new StarIOPortException("OUT OF PAPER");
			} else if (true == status.offline) {
				throw new StarIOPortException("PRINTER IS OFF");
			}

			if(callbackContext != null){
				callbackContext.success("" + orderId + "|Printing Successful");

				// callbackId.success();
			}
		} catch (StarIOPortException e) {
			result = false;
			e.printStackTrace();

			if(callbackContext != null){
				callbackContext.error("" + orderId + "|" + e.getMessage());
			}
		} finally {
			if (port != null) {
				try {
					StarIOPort.releasePort(port);
				} catch (StarIOPortException e) {
				}
			}
		}

		return result;
	}

	public static String repeat(int count, String with) {
		count = Math.max(1, count);
		return new String(new char[count]).replace("\0", with);
	}

	public static String repeat(int count) {
		return repeat(count, " ");
	}

  public void printTestReceipt(String ipAddress, CallbackContext callbackId) throws Exception {
    System.out.println("PRINT TEST RECEIPT. ipAddress: " + ipAddress);
    String portName = "TCP:"+ipAddress;

    ArrayList<byte[]> list = new ArrayList<byte[]>();

    RasterDocument rasterDoc = new RasterDocument(RasSpeed.Medium, RasPageEndMode.FeedAndFullCut, RasPageEndMode.FeedAndFullCut, RasTopMargin.Standard, 0, 0, 0);
    list.add(rasterDoc.BeginDocumentCommandData());

    String textToPrint = ("Printer Test:");
    list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, 0, Layout.Alignment.ALIGN_CENTER));

    textToPrint = ("Success!");
    list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, 0, Layout.Alignment.ALIGN_CENTER));

    list.add(rasterDoc.EndDocumentCommandData());

    sendCommand(callbackId, MainActivity.this, TEST_ORDER_ID, portName, portSettings, list);
  }

	public void printReceipt(String ipAddress, JSONObject receipt, boolean isBluetooth, CallbackContext callbackId) throws Exception {
        System.out.println("PRINT RECEIPT. ipAddress: " + ipAddress);
		String portName = (isBluetooth ? "BT:" : ("TCP:" + ipAddress));
		// String portName = "BT:";
		
		ArrayList<byte[]> list = new ArrayList<byte[]>();

		// receipt 		  	= new JSONObject(receiptObject);
		int orderId					 = Integer.parseInt(receipt.getString("id"));
		String receiptOrderNumber 	 = receipt.getString("orderNumber");
		String receiptTimePlaced  	 = receipt.getString("timePlacedFormatted");
		String receiptDatePlaced  	 = receipt.getString("datePlacedFormatted");
		String receiptStationLabel   = receipt.getString("stationName");
		String receiptAisleLabel  	 = receipt.getString("aisleName");
		String receiptSection 	  	 = receipt.getString("section");
		String receiptRow		  	     = receipt.getString("row");
		String receiptSeat		  	   = receipt.getString("seat");
		String receiptName		  	   = receipt.getString("name");
		String receiptNumber	  	   = receipt.getString("number");
		String receiptSubtotal    	 = receipt.getString("subtotal");
		String receiptProcessingFee  = receipt.getString("processingFee");
		String receiptDeliveryFee 	 = receipt.getString("deliveryFee");
        String receiptDollarsToSubtract    = receipt.getString("dollarsToSubtract");
		String receiptTotal		  	   = receipt.getString("totalPrice");
		String receiptRunner  	 	   = receipt.getString("runner");
        int receiptIsPrepaid         = receipt.getInt("isPrepaid");
		int receiptIsDelivery  	 	   = receipt.getInt("isDelivery");
        int receiptIsPriority          = receipt.getInt("isPriority");
		int receiptIsCreditCardOrder = receipt.getInt("isCreditCardOrder");
		JSONArray receiptItems	  	 = receipt.getJSONArray("items");
        String freeItemName          = receipt.getString("freeItemName");
    

		RasterDocument rasterDoc = new RasterDocument(RasSpeed.Medium, RasPageEndMode.FeedAndFullCut, RasPageEndMode.FeedAndFullCut, RasTopMargin.Standard, 0, 0, 0);
		list.add(rasterDoc.BeginDocumentCommandData());

//			Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.grubrunner_logo_574px);
//			StarBitmap starbitmap = new StarBitmap(bm, false, 574);
//			list.add(starbitmap.getImageRasterDataForPrinting(true));
//            
//            
//           

		String textToPrint;

        // PRIORITY
        if(receiptIsPriority == 1){
            textToPrint = (
                "★★★★★★★★★"
            );
            list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

            textToPrint = (
                "PRIORITY"
            );
            list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

            textToPrint = (
                "★★★★★★★★★\r\n\r\n"
            );
            list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));
        }



        textToPrint = ("------------------------------------");
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0, Layout.Alignment.ALIGN_CENTER));

        textToPrint = ("-          PLACE CARD IN           -");
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0, Layout.Alignment.ALIGN_CENTER));

        textToPrint = ("*         ENVELOPE & PASS          *");
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0, Layout.Alignment.ALIGN_CENTER));

        textToPrint = ("-          DOWN TO RUNNER          -");
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0, Layout.Alignment.ALIGN_CENTER));

        textToPrint = ("------------------------------------\r\n");
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0, Layout.Alignment.ALIGN_CENTER));

        

        textToPrint = (receiptName);
        list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, 0));

        textToPrint = "ORDER: " + receiptOrderNumber;
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));

        textToPrint = "STATION: " + receiptStationLabel;
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));

        textToPrint = (receiptDatePlaced + repeat(NUM_LINES_SMALL - 1 - receiptDatePlaced.length() - receiptTimePlaced.length()) + receiptTimePlaced);
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));


        textToPrint = ("\r\nITEMS");
        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));

        // Print items and prices
        try{
        for (int i=0; i < receiptItems.length(); i++) {
            try{
                JSONObject itemObject = receiptItems.getJSONObject(i);
                String quantity = itemObject.getString("quantity");
                String item     = itemObject.getString("item").toUpperCase();
                String price = "0";
                try{
                    price = itemObject.getString("price");
                }catch(Exception e){}

                DecimalFormat df = new DecimalFormat("0"); // Use DecimalFormat("0.00") if you want to include cents
                String total = df.format(Integer.parseInt(quantity) * Double.parseDouble(price));

                textToPrint = " " + quantity + " " + item + repeat(NUM_LINES_SMALL - 2 - quantity.length() - item.length() - total.length() - 1) + '$' + total;

                list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, TYPEFACE_BOLD));
            }catch(Exception e){}
        }
        }catch(Exception e){}

        if(freeItemName != null && !freeItemName.equals("null") && freeItemName.length() > 0){
          String postFix = " FREE";
          textToPrint = freeItemName + repeat(NUM_LINES_SMALL - freeItemName.length() - postFix.length()) + postFix;
          list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, TYPEFACE_BOLD));
        }

        textToPrint = "SUBTOTAL" + repeat(NUM_LINES_SMALL - 8 - 1 - (receiptSubtotal.length() + 1)) + '$' + receiptSubtotal + "\r\n";
        if(!receiptProcessingFee.equals("0.00") && receiptProcessingFee.equals("0")) {
            textToPrint += "PROCESSING FEE" + repeat(NUM_LINES_SMALL - 14 - 1 - (receiptProcessingFee.length() + 1)) + '$' + receiptProcessingFee + "\r\n";
        }
        textToPrint += "DELIVERY FEE" + repeat(NUM_LINES_SMALL - 12 - 1 - (receiptDeliveryFee.length() + 1)) + '$' + receiptDeliveryFee + "\r\n";

        // Only add the refund line if the refund is greater than 0
        if(Integer.parseInt(receiptDollarsToSubtract) > 0){
            textToPrint += "REFUND" + repeat(NUM_LINES_SMALL - 6 - 1 - (receiptDollarsToSubtract.length() + 1)) + "-$" + receiptDollarsToSubtract + "\r\n";
        }

        textToPrint += "-----\r\n";
        textToPrint += "TOTAL" + repeat(NUM_LINES_SMALL - 7 - receiptTotal.length()) + "$" + receiptTotal + "\r\n";
        textToPrint += "------------------------------------";

        list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));



        if(receiptIsDelivery == 0){
            // 
            // Order for pickup
            // 
            textToPrint = (
                    "** PICKUP **");

            list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

            if(receiptIsCreditCardOrder == 0){
                textToPrint = (
                    "UNPAID: $" + receiptTotal
                );

                list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));
            }
        }else {
            //
            // Order is a delivery, and order was done by Face-to-Face
            //
            //
            
            textToPrint = ("\r\nAISLE");

            list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, 0, Layout.Alignment.ALIGN_CENTER));

            textToPrint = receiptAisleLabel;
            // If ANY of SEC/ROW/SEAT are empty, then aisle should be empty
            if((receiptSection.length() * receiptRow.length() * receiptSeat.length()) == 0){
                textToPrint = "";   
            }

            list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

            textToPrint = (
                    "------------------------------------");


            list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));

            textToPrint = ("SEC         ROW         SEAT");

            list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, 0, Layout.Alignment.ALIGN_CENTER));

            textToPrint = (
                    receiptSection +
                            repeat(7 - receiptSection.length() - (int)Math.floor(receiptRow.length()/2.0)) +
                            receiptRow +
                            repeat(7 - (int)Math.ceil(receiptRow.length()/2.0) - receiptSeat.length()) + receiptSeat
            );

            list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

            textToPrint = (
                "------------------------------------\r\n"
            );

            list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));

            textToPrint = ("TOTAL: $" + receiptTotal);
            list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));
        } 

        // PRIORITY
        if(receiptIsPriority == 1){
            textToPrint = (
                "\r\n\r\n★★★★★★★★★"
            );
            list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

            textToPrint = (
                "PRIORITY"
            );
            list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

            textToPrint = (
                "★★★★★★★★★\n\n"
            );
            list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));
        }


        /*
        // DEPRECATED because F2F orders no longer have assigned runners
        if(receiptRunner.equals("") || receiptRunner.equals("null")) {
			// 
			// Customer who is NOT in the student section, ordered through SMS and paid with credit card
			// 
			if(!receiptAisleLabel.equals("")) {
				textToPrint = (
						"\r\nAISLE");

				list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, 0, Layout.Alignment.ALIGN_CENTER));

				textToPrint = receiptAisleLabel;

				list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));
			}

			textToPrint = (
					"------------------------------------");

			list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));

			textToPrint = (
					"SEC         ROW         SEAT");

			list.add(createRasterCommand(textToPrint, TEXTSIZE_MEDIUM, 0, Layout.Alignment.ALIGN_CENTER));

			textToPrint = (
					receiptSection +
							repeat(7 - receiptSection.length() - (int)Math.floor(receiptRow.length()/2.0)) +
							receiptRow +
							repeat(7 - (int)Math.ceil(receiptRow.length()/2.0) - receiptSeat.length()) + receiptSeat
			);

			list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

			textToPrint = (
					"------------------------------------\r\n");

			list.add(createRasterCommand(textToPrint, TEXTSIZE_SMALL, 0));

            textToPrint = ("TEXT MESSAGE\n");

            list.add(createRasterCommand(textToPrint, TEXTSIZE_LARGER, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));

            if(receiptIsPrepaid == 1){
                textToPrint = ("** PRE-PAID **");
            }else{
                textToPrint = ("UNPAID: $" + receiptTotal);
            }

            list.add(createRasterCommand(textToPrint, TEXTSIZE_EXTRA_LARGE, TYPEFACE_BOLD, Layout.Alignment.ALIGN_CENTER));
		}
        */

         

		list.add(rasterDoc.EndDocumentCommandData());

		list.add(new byte[] { 0x07 }); // Kick cash drawer

		sendCommand(callbackId, MainActivity.this, orderId, portName, portSettings, list);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		System.out.println("\n\n*** MainActivity.onCreate");
		// setContentView(R.layout.activity_main);

		MyBus.getInstance().register(this);
	}

	@Override protected void onDestroy() {
		System.out.println("\n\n*** MainActivity.onDestroy");
		MyBus.getInstance().unregister(this);
		super.onDestroy();
	}

	@Subscribe public void onAsyncTaskResult(AsyncTaskResultEvent event) {
		System.out.println("\n\n*** MainActivity.onAsyncTaskResult");
		Toast.makeText(this, event.getResult(), Toast.LENGTH_LONG).show();
	}
}
